package StepLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class LoginValidData {
    WebDriver driver;

    @Given("^User go to login page$")
    public void User_go_to_login_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://qa.cilsy.id:8080/en/login?back=my-account");
    }

    @When("^User enter valid userID and password$")
    public void User_enter_valid_userID_and_password() throws Throwable {
        driver.findElement(By.id("email")).sendKeys("nurul.qawork@gmail.com");
        driver.findElement(By.id("passwd")).sendKeys("Nurul123");
        Thread.sleep(1000);
    }

    @Then("^User is navigated to homepage$")
    public void User_is_navigated_to_homepage() throws Throwable {
        System.out.println("This step user login successful");
        WebDriverWait wait = new WebDriverWait(driver, 5);
        driver.findElement(By.id("SubmitLogin")).click();
        Thread.sleep(1000);
        String ActualTitle = driver.getTitle();
        String ExpectedTitle = "My account - Small Project QA13";
        Assert.assertEquals(ExpectedTitle, ActualTitle);

        Thread.sleep(1000);
        driver.quit();

    }
}
