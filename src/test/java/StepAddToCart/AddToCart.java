package StepAddToCart;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AddToCart {
    WebDriver driver;

    WebElement radioBtn;

    @Given("^Go to hotel list page$")
    public void Go_to_hotel_list_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://qa.cilsy.id:8080/en/6-the-hotel-prime?date_from=2022-07-13&date_to=2022-07-14");
    }

    @When("^User click book now button$")
    public void User_click_book_now_button() throws Throwable{
        driver.findElement(By.xpath("//*[@id=\"category_data_cont\"]/div[1]/div/div[2]/a")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"category_data_cont\"]/div[2]/div/div[2]/a")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/span/span")).click();
        Thread.sleep(1000);

    }

    @Then("^Room successful added to cart$")
    public void Room_successful_added_to_cart() throws Throwable{
        System.out.println("This step user add room to cart ");
        driver.findElement(By.className("shopping_cart")).click();
        Thread.sleep(1000);
        String ActualTitle = driver.getTitle();
        String ExpectedTitle = "Order - Small Project QA13";
        Assert.assertEquals(ExpectedTitle, ActualTitle);

        Thread.sleep(1000);
        driver.quit();
    }
}
