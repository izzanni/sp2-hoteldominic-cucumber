package StepSearch;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class Search {
    WebDriver driver;



    @Given("^Go to homepage$")
    public void Go_to_homepage() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://qa.cilsy.id:8080/en/");
    }

    @When("^User enter the information$")
    public void User_enter_the_information() throws Throwable{
        driver.findElement(By.id("hotel_location")).sendKeys("Aceh");
        Thread.sleep(500);
        //select hotel
        driver.findElement(By.id("id_hotel_button")).click();
        driver.findElement(By.xpath("//*[@id=\"search_hotel_block_form\"]/div[2]/div/ul")).click();
        Thread.sleep(500);
        //select check in date
        driver.findElement(By.id("check_in_time")).click();
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[3]/a")).click();
        Thread.sleep(500);
        //select check out date
        driver.findElement(By.id("check_out_time")).click();
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[4]/a")).click();

    }

    @Then("^List of hotel appear$")
    public void List_of_hotel_appear() throws Throwable{
        System.out.println("This step user search available hotel ");
        driver.findElement(By.id("search_room_submit")).click();
        Thread.sleep(1000);
        String ActualTitle = driver.getTitle();
        String ExpectedTitle = "The Hotel Prime - Small Project QA13";
        Assert.assertEquals(ExpectedTitle, ActualTitle);

        Thread.sleep(1000);
        driver.quit();
    }
}
