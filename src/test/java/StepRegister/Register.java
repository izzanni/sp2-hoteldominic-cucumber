package StepRegister;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;


public class Register {
    WebDriver driver;

    WebElement radioBtn;


    @Given("^Launch the application and click sign in$")
    public void Launch_the_application_and_click_sign_in() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://qa.cilsy.id:8080/en/");
        driver.findElement(By.linkText("Sign in")).click();
    }

    @When("^User enter email address$")
    public void User_enter_email_address() throws Throwable{
        driver.findElement(By.id("email_create")).sendKeys("testing12@mailnator.com");
        Thread.sleep(1000);
        driver.findElement(By.id("SubmitCreate")).click();
        Thread.sleep(2000);
    }

    @And("^Fill the registration form$")
    public void Fill_the_registration_form() throws Throwable{
        radioBtn = driver.findElement(By.id("id_gender2"));
        radioBtn.click();
        driver.findElement(By.id("customer_firstname")).sendKeys("nurul");
        driver.findElement(By.id("customer_lastname")).sendKeys("i");
        driver.findElement(By.id("passwd")).sendKeys("Nurul123");
        //select day
        driver.findElement(By.id("days")).click();
        Select day = new Select(driver.findElement(By.id("days")));
        day.selectByValue("10");
        //select month
        driver.findElement(By.id("months")).click();
        Select months = new Select(driver.findElement(By.id("months")));
        months.selectByValue("5");
        //select year
        driver.findElement(By.id("years")).click();
        Select years = new Select(driver.findElement(By.id("years")));
        years.selectByValue("1995");
    }

    @Then("^Account successful register$")
    public void Account_successful_register() throws Throwable{
        System.out.println("This step user successfully register account ");
        driver.findElement(By.id("submitAccount")).click();
        Thread.sleep(1000);
        String ActualTitle = driver.getTitle();
        String ExpectedTitle = "My account - Small Project QA13";
        Assert.assertEquals(ExpectedTitle, ActualTitle);

        Thread.sleep(1000);
        driver.quit();
    }
}
