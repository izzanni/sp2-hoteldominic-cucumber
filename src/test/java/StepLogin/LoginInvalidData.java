package StepLogin;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class LoginInvalidData {
    WebDriver driver;


    @Given("^User navigates to login page$")
    public void User_navigates_to_login_page() throws Throwable {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("http://qa.cilsy.id:8080/en/login?back=my-account");
    }

    @When("^User enter invalid userID and password$")
    public void User_enter_invalid_userID_and_password() throws Throwable {
        driver.findElement(By.id("email")).sendKeys("nurul.qawork@gmail.com");
        driver.findElement(By.id("passwd")).sendKeys("Nurul");
        Thread.sleep(1000);
    }

    @Then("^Wrong message pop up$")
    public void Wrong_message_pop_up() throws Throwable {
        System.out.println("This step user login fail");
        driver.findElement(By.name("SubmitLogin")).click();
        WebDriverWait wait = new WebDriverWait(driver, 5);
        WebElement id = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"center_column\"]/div[1]")));
        assertTrue(id.isDisplayed());
        Thread.sleep(1000);
        driver.quit();
    }
}
