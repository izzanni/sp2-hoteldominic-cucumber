package StepCheckout;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;



public class Checkout {
    WebDriver driver;
    WebDriverWait wait;

    @Given("^Open login page$")
    public void Open_login_page() throws Throwable {
        //    System.out.println("this step Open the chrome launch the application and login page");
        System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("http://qa.cilsy.id:8080/en/");
        driver.findElement(By.linkText("Sign in")).click();

        Thread.sleep(500);
        driver.findElement(By.id("email")).sendKeys("testing4@mailnator.com");
        driver.findElement(By.id("passwd")).sendKeys("Nurul123");
        driver.findElement(By.id("SubmitLogin")).click();
        Thread.sleep(1000);
    }

    @When("^User Input Room Information$")
    public void User_Input_Room_Information() throws Throwable {
        //    System.out.println("This step User Input Location SelectHotel CheckInDate CheckOutDate");
        driver.findElement(By.className("nav_toggle")).click();
        Thread.sleep(500);
        driver.findElement(By.className("navigation-link")).click();
        Thread.sleep(500);
        driver.findElement(By.id("hotel_location")).sendKeys("Aceh");
        Thread.sleep(1000);
        //select hotel
        driver.findElement(By.id("id_hotel_button")).click();
        driver.findElement(By.xpath("//*[@id=\"search_hotel_block_form\"]/div[2]/div/ul")).click();
        Thread.sleep(1000);
        //select check in date
        driver.findElement(By.id("check_in_time")).click();
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[3]/a")).click();
        Thread.sleep(500);
        //select check out date
        driver.findElement(By.id("check_out_time")).click();
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[4]/a")).click();
        Thread.sleep(1000);
        driver.findElement(By.id("search_room_submit")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"category_data_cont\"]/div[1]/div/div[2]/a")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"layer_cart\"]/div[1]/div[2]/div[4]/a")).click();
        Thread.sleep(1000);
    }

    @And("^User input address$")
    public void User_input_address() throws Throwable {
        //System.out.printIn("This step user input addres and save address");
        driver.findElement(By.id("company")).sendKeys("Sekolah QA");
        Thread.sleep(500);
        driver.findElement(By.id("vat-number")).sendKeys("123456");
        Thread.sleep(500);
        driver.findElement(By.id("address1")).sendKeys("Shah");
        Thread.sleep(500);
        driver.findElement(By.id("address2")).sendKeys("Alam");
        Thread.sleep(500);
        driver.findElement(By.id("postcode")).sendKeys("40460");
        Thread.sleep(500);
        driver.findElement(By.id("city")).sendKeys("Selangor");
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"id_state\"]/option[67]")).click();
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"id_country\"]/option")).click();
        Thread.sleep(500);
        driver.findElement(By.id("phone")).sendKeys("01837199527");
        Thread.sleep(500);
        driver.findElement(By.id("phone_mobile")).sendKeys("0183719952");
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"alias\"]")).sendKeys("My address");
        Thread.sleep(500);
        driver.findElement(By.xpath("//*[@id=\"submitAddress\"]")).click();
        Thread.sleep(1000);
    }

    @Then("^User proceed to checkout$")
    public void User_proceed_to_checkout() throws Throwable {
        //    System.out.println("This step user proceed to checkout");
        //Rooms & Price Summary
        driver.findElement(By.xpath("//*[@id=\"collapse-shopping-cart\"]/div/div[2]/div[2]/div/a")).click();
        Thread.sleep(1000);
        //Guest Information
        driver.findElement(By.xpath("//*[@id=\"collapse-guest-info\"]/div/div[4]/div/a")).click();
        Thread.sleep(1000);
        //Payment Information
        driver.findElement(By.id("cgv")).click();
        Thread.sleep(2000);
        //Pay bay bank wire
        driver.findElement(By.className("bankwire")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"cart_navigation\"]/button")).click();
        Thread.sleep(1000);

        //order history
        driver.findElement(By.xpath("//*[@id=\"center_column\"]/p/a")).click();

        Thread.sleep(2000);
        driver.quit();

    }
}
